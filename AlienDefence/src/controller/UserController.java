package controller;

import model.User;
import model.persistance.IUserPersistance;
import model.persistanceDB.UserDB;

/**
 * controller for users
 * @author Anton Gusek
 * TODO implement this class
 */
public class UserController {
	//Erstellen der Klasse userDB @author Anton Gusek
	private UserDB userDB;
	
	
	//Deklarieren der Klasse userDB
	public UserController(UserDB userDB) {
		this.userDB = userDB;
	}
	
	//Der Datenbank wird ein weiterer User hinzugef�gt. Hierf�r wird die dazugeh�rige Methode aufgerufen.
	public void createUser(User user) {
		userDB.createUser(user);
	}
	
	//Liest einen User aus der Datenbank aus, mit dem Dazugeh�rigen Namen
	public User readUser(String username) {
		return userDB.readUser(username);
	}
	
	//Schreibt den ver�nderten User in die Datenbank
	public void changeUser(User user) {
		userDB.updateUser(user);
		
	}
	
	//L�scht einen User, hief�r wird der User �bergeben und anhand seiner ID, kann man ihn entfernen. @author Anton Gusek
	public void deleteUser(User user) {
		userDB.deleteUser(user);
	}
	
	//Es wird �berpr�ft ob das eingegebene PW, mit dem aus der Datenbank �bereinstimmt. Wenn ja wird true zur�ck gegeben.
	public boolean checkPassword(String username, String passwort) {
		
		User user = this.readUser(username);
		if(user.getPassword().equals(passwort)) {
			return true;
		}
		
		
		return false;
	}
}
