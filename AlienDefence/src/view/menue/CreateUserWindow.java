package view.menue;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.LevelController;
import model.Level;
import model.User;
import model.persistanceDB.AccessDB;
import model.persistanceDB.UserDB;

import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;

//TODO create a usermanagement

public class CreateUserWindow extends JFrame{
	
	private JTextField txtmarital;
	private JTextField txtFirst;
	private JTextField txtsalary;
	private JTextField txtbirth;
	private JTextField txtcity;
	private JTextField txtstreet;
	private JTextField txtloginname;
	private JTextField txtpostal;
	private JTextField txthouse;
	private JTextField txtSur;
	private JTextField txtfinal;
	private JTextField txtPW;
	
	public CreateUserWindow() {
		getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblFirst = new JLabel("Vorname");
		getContentPane().add(lblFirst);
		
		txtFirst = new JTextField();
		txtFirst.setColumns(10);
		getContentPane().add(txtFirst);
		
		JLabel lblSur = new JLabel("Nachname");
		getContentPane().add(lblSur);
		
		txtSur = new JTextField();
		txtSur.setColumns(10);
		getContentPane().add(txtSur);
		
		JLabel lblbirth = new JLabel("Geburtstag");
		getContentPane().add(lblbirth);
		
		txtbirth = new JTextField();
		txtbirth.setColumns(10);
		getContentPane().add(txtbirth);
		
		JLabel lblstreet = new JLabel("Stra\u00DFe");
		getContentPane().add(lblstreet);
		
		txtstreet = new JTextField();
		txtstreet.setColumns(10);
		getContentPane().add(txtstreet);
		
		JLabel lblhouse = new JLabel("Hausnummer");
		getContentPane().add(lblhouse);
		
		txthouse = new JTextField();
		txthouse.setColumns(10);
		getContentPane().add(txthouse);
		
		JLabel lblpostal = new JLabel("Postleitzahl");
		getContentPane().add(lblpostal);
		
		txtpostal = new JTextField();
		txtpostal.setColumns(10);
		getContentPane().add(txtpostal);
		
		JLabel lblcity = new JLabel("Stadt");
		lblcity.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblcity);
		
		txtcity = new JTextField();
		txtcity.setColumns(10);
		getContentPane().add(txtcity);
		
		JLabel lblloginname = new JLabel("Benutzername");
		getContentPane().add(lblloginname);
		
		txtloginname = new JTextField();
		txtloginname.setColumns(10);
		getContentPane().add(txtloginname);
		
		JLabel lblPW = new JLabel("Passwort");
		getContentPane().add(lblPW);
		
		txtPW = new JTextField();
		getContentPane().add(txtPW);
		txtPW.setColumns(10);
		
		JLabel lblsalary = new JLabel("Gehalts Wunsch");
		getContentPane().add(lblsalary);
		
		txtsalary = new JTextField();
		txtsalary.setColumns(10);
		getContentPane().add(txtsalary);
		
		JLabel lblmarital = new JLabel("Beziehung");
		getContentPane().add(lblmarital);
		
		txtmarital = new JTextField();
		getContentPane().add(txtmarital);
		txtmarital.setColumns(10);
		
		JLabel lblfinal = new JLabel("Abschlussnote");
		getContentPane().add(lblfinal);
		
		txtfinal = new JTextField();
		getContentPane().add(txtfinal);
		txtfinal.setColumns(10);
		
		JButton btnAbschicken = new JButton("Best\u00E4tigen");
		btnAbschicken.setForeground(Color.GRAY);
		btnAbschicken.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (txtFirst.getText().equals("") || txtSur.getText().equals("") || txtbirth.getText().equals("") || txtstreet.getText().equals("") || txthouse.getText().equals("") || txtpostal.getText().equals("") || txtcity.getText().equals("") || txtloginname.getText().equals("") || txtPW.getText().equals("") || txtsalary.getText().equals("") || txtmarital.getText().equals("") || txtfinal.getText().equals("")) {
					btnAbschicken.setBackground(Color.red);
				}else {
					LocalDate date = LocalDate.parse(txtbirth.getText());
					btnAbschicken.setBackground(Color.green);
					User user = new User(1, txtFirst.getText(), txtSur.getText(),date, txtstreet.getText(), txthouse.getText(), txtpostal.getText(),txtcity.getText(), txtloginname.getText(), txtPW.getText(), Integer.parseInt(txtsalary.getText()) , txtmarital.getText(), Double.parseDouble(txtfinal.getText()));
					
					AccessDB dbAccess = new AccessDB();
					UserDB userDB = new UserDB(dbAccess);
					userDB.createUser(user);
				}
			}
		});
		getContentPane().add(btnAbschicken);
		
		this.setVisible(true);
	}
	
    
}
